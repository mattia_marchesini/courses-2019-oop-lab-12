﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            this.values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
             foreach(TKey1 k1 in keys1)
            {
                foreach(TKey2 k2 in keys2)
                {
                    this.values.Add(new Tuple<TKey1, TKey2>(k1, k2), generator.Invoke(k1, k2));
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {

            if (other.NumberOfElements != this.NumberOfElements)
            {
                return false;
            }

            foreach(Tuple <TKey1, TKey2, TValue> t in other.GetElements())
            {
                Tuple<TKey1, TKey2> key = new Tuple<TKey1, TKey2>(t.Item1, t.Item2);
                KeyValuePair<Tuple<TKey1, TKey2>, TValue> pair = new KeyValuePair<Tuple<TKey1, TKey2>, TValue>(key, t.Item3);
                if (!this.values.Contains(pair))
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */
            get
            {
                return this.values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                this.values.Add(new Tuple<TKey1, TKey2>(key1, key2), value);
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> t in values.Keys)
            {
                list.Add(new Tuple<TKey1, TKey2, TValue>(t.Item1,t.Item2,this[t.Item1, t.Item2]));
            }
            return list;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
